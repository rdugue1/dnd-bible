package com.ralphdugue.pocketfanbrew.di.modules


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dev.ralphdugue.fanbrewdata.PocketFanbrewRepos
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideClassRepository(): ClassRepository = PocketFanbrewRepos.OPEN5E.classesRepository
}