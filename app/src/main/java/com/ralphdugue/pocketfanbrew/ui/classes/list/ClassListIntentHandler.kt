package com.ralphdugue.pocketfanbrew.ui.classes.list

import android.util.Log
import com.ralphdugue.phitoarch.mvi.BaseIntentHandler
import com.ralphdugue.phitoarch.mvi.ViewState
import com.ralphdugue.pocketfanbrew.domain.usecases.classes.ObserveListOfClasses
import com.ralphdugue.pocketfanbrew.domain.usecases.classes.UpdateListOfClasses
import dev.ralphdugue.fanbrewdata.common.ApiError
import dev.ralphdugue.fanbrewdata.common.ApiException
import dev.ralphdugue.fanbrewdata.common.ApiSuccess
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ClassListIntentHandler @Inject constructor(
    private val observeListOfClasses: ObserveListOfClasses,
    private val updateListOfClasses: UpdateListOfClasses
): BaseIntentHandler<ClassListEvent> {
    override fun process(event: ClassListEvent, currentState: ViewState): Flow<ViewState> = when (event) {
        ClassListEvent.WatchClasses -> observeListOfClasses.execute(Any())
            .map {
                if (it.isEmpty()) Empty else ShowClasses(it)
            }
        ClassListEvent.UpdateClasses -> flow {
            emit(Loading)
            when (val result = updateListOfClasses.execute(Any())) {
                is ApiError -> throw LoadClassesException(Throwable(result.message))
                is ApiException -> throw LoadClassesException(result.e)
                is ApiSuccess -> Log.d("API_RESPONSE_SUCCESS", result.toString())
            }
        }
    }
    data class LoadClassesException(override val cause: Throwable): Exception(cause)
}