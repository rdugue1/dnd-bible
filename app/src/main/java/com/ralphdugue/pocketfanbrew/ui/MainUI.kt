package com.ralphdugue.pocketfanbrew.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.ralphdugue.pocketfanbrew.ui.destinations.Destination
import com.ralphdugue.pocketfanbrew.ui.navigation.*
import com.ralphdugue.pocketfanbrew.ui.theme.FanbrewTheme
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec
import com.ramcosta.composedestinations.utils.currentDestinationAsState
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    MainContent(
        title = "Test",
        onMenuItemSelected = { /*TODO*/ }
    ) {
        
    }
}

@ExperimentalMaterialApi
@Composable
fun DnDBibleApp() {
    FanbrewTheme {
        val navController = rememberNavController()
        Surface(color = MaterialTheme.colors.background) {
            MainScreen(navController)
        }
    }
}

@ExperimentalMaterialApi
@Composable
private fun MainScreen(navController: NavHostController) {
    val destination = navController.currentDestinationAsState().value
        ?: NavGraphs.root.startAppDestination
    MainContent(
        title = destinationTitle(destination as Destination),
        onMenuItemSelected = { directions -> navController.navigate(directions.route) }
    ) { modifier ->
        DestinationsNavHost(
            navGraph = NavGraphs.root,
            modifier = modifier,
            navController = navController
        )
    }
}

@ExperimentalMaterialApi
@Composable
private fun MainContent(
    title: String,
    onMenuItemSelected: (DirectionDestinationSpec) -> Unit,
    body: @Composable (modifier: Modifier) -> Unit
) {
    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    Scaffold(
        scaffoldState = scaffoldState,
        drawerContent = {
                 NavMenuScreen {
                     onMenuItemSelected(it)
                     scope.launch {
                         scaffoldState.drawerState.apply {
                             if (isClosed) open() else close()
                         }
                     }
                 }
        },
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = title,
                        fontFamily = FontFamily(Font(com.ralphdugue.pocketfanbrew.R.font.quentin_caps, FontWeight.Normal))
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = { scope.launch { scaffoldState.drawerState.open() } }) {
                        Icon(Icons.Filled.Menu, contentDescription = null)
                    }
                },
                elevation = 8.dp
            )
        }
    ) { innerPadding ->
        body(Modifier.padding(innerPadding))
    }
}

