package com.ralphdugue.pocketfanbrew.ui.classes.list

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.ralphdugue.phitoarch.mvi.ViewState
import com.ralphdugue.pocketfanbrew.domain.entities.classes.ClassListItemData
import com.ralphdugue.pocketfanbrew.ui.common.AddGridItem
import com.ralphdugue.pocketfanbrew.ui.common.GridItem
import com.ralphdugue.pocketfanbrew.ui.theme.FanbrewTheme
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph

@Preview(device = Devices.PIXEL_4_XL)
@Composable
private fun GridItemPreview() {
    FanbrewTheme {
        ClassListContent(
            listOf(
                ClassListItemData(
                    name = "Barbarian",
                    imageUrl = "",
                    description = "Boring and predictable"
                ),
                ClassListItemData(
                    name = "Ranger",
                    imageUrl = "",
                    description = "Really really super cool, think Legolas. A true warrior and gentleman."
                ),
                ClassListItemData(
                    name = "Rogue",
                    imageUrl = "",
                    description = "Boring and predictable"
                ),
                ClassListItemData(
                    name = "Paladin",
                    imageUrl = "",
                    description = "Boring and predictable"
                )
            )
        )
    }
}

@RootNavGraph(start = true)
@Destination
@Composable
fun ClassListScreen() {
    val viewModel = hiltViewModel<ClassListViewModel>()
    val state by remember { viewModel.state }
    when (state) {
        is Empty -> NoClasses("NO CLASSES FOUND")
        is ViewState.Error -> NoClasses("THERE WAS AN ERROR")
        is ShowClasses -> ClassListContent(classes = (state as ShowClasses).classes)
        is Loading -> NoClasses("Loading...")
    }
   //viewModel.queueEvent(ClassListEvent.WatchClasses)
}

@Composable
private fun NoClasses(message: String) {
    Box(
        modifier = Modifier.fillMaxSize(),
    ) {
        Text(
            text = message,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun ClassListContent(classes: List<ClassListItemData>) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(3),
    ) {
        items(classes) { class_ ->
            GridItem(
                imageUrl = class_.imageUrl,
                title = class_.name,
                details = class_.description
            )
        }
        item {
            AddGridItem()
        }
    }
}