package com.ralphdugue.pocketfanbrew.ui.navigation

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ralphdugue.pocketfanbrew.R
import com.ralphdugue.pocketfanbrew.ui.theme.FanbrewTheme
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec
import java.util.*

@ExperimentalMaterialApi
@Preview
@Composable
private fun Preview() {
    FanbrewTheme {
        NavMenuContent(DrawerDestinations.values()) {}
    }
}

@ExperimentalMaterialApi
@Composable
fun NavMenuScreen(onMenuItemSelected: (DirectionDestinationSpec) -> Unit) {
    val menu = DrawerDestinations.values()
    NavMenuContent(menu) { onMenuItemSelected(it) }
}

@ExperimentalMaterialApi
@Composable
fun NavMenuContent(
    menu: Array<DrawerDestinations>,
    onMenuItemSelected: (DirectionDestinationSpec) -> Unit
) {
    Column {
        NavHeader()
        Divider(
            color = MaterialTheme.colors.primary,
            thickness = 1.dp
        )
        menu.forEach { item ->
            with(item) {
                NavMenuItem(
                    name = label,
                    iconRes = iconRes
                ) { onMenuItemSelected(direction) }
            }
        }
    }
}


@Composable
private fun NavHeader() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.secondary)
            .height(200.dp),
        contentAlignment = Alignment.Center
    ) {
        Spacer(
            Modifier
                .fillMaxWidth()
                .height(2.dp)
                .background(MaterialTheme.colors.primary)
        )
        Image(
            painter = painterResource(id = R.drawable.pocke_tt_logo),
            contentDescription = "Fanbrew Logo",
            colorFilter = ColorFilter.tint(MaterialTheme.colors.primary)
        )
    }
}



@ExperimentalMaterialApi
@Composable
private fun  NavMenuItem(
    name: String,
    @DrawableRes iconRes: Int,
    onMenuItemSelected: () -> Unit
) {
    Column {
        ListItem(
            text = {
                Text(
                    text = name.uppercase(Locale.getDefault()),
                    color = MaterialTheme.colors.onBackground,
                    style = MaterialTheme.typography.h1
                )
            },
            icon = {
                Icon(
                    imageVector = ImageVector.vectorResource(id = iconRes),
                    contentDescription = null,
                    modifier = Modifier.size(50.dp),
                    tint = MaterialTheme.colors.onBackground
                )
            },
            modifier = Modifier
                .clickable { onMenuItemSelected() }
                .padding(16.dp)
        )
        Spacer(
            Modifier
                .fillMaxWidth()
                .height(1.dp)
        )
    }
}