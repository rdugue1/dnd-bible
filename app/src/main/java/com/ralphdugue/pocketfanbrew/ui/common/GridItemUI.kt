package com.ralphdugue.pocketfanbrew.ui.common

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.ralphdugue.pocketfanbrew.R
import com.ralphdugue.pocketfanbrew.ui.theme.FanbrewTheme

@Preview
@Composable
private fun GridItemPreview() {
    FanbrewTheme {
        //GridItem(imageUrl = "", title = "Ranger", details = "A range rogue")
        AddGridItem()
    }
}

@Composable
fun GridItem(
    imageUrl: String,
    title: String,
    details: String
) {
    Card(
        modifier = Modifier
            .padding(5.dp)
            .height(183.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp
    ) {
        Column(modifier = Modifier.padding(10.dp)) {
            AsyncImage(
                modifier = Modifier
                    .height(90.dp)
                    .align(Alignment.CenterHorizontally),
                model = ImageRequest.Builder(LocalContext.current)
                    .data(imageUrl)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(R.drawable.pocke_tt_logo),
                contentDescription = details,
                colorFilter = ColorFilter.tint(color = MaterialTheme.colors.secondary)
            )
            Spacer(modifier = Modifier.padding(1.dp))
            Divider(color = MaterialTheme.colors.primaryVariant)
            Spacer(modifier = Modifier.padding(2.dp))
            ResponsiveText(text = title, style = MaterialTheme.typography.h1)
            Text(
                modifier = Modifier
                    .height(90.dp)
                    .align(Alignment.Start),
                text = details,
                style = MaterialTheme.typography.body1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
fun AddGridItem() {
    Card(
        modifier = Modifier
            .padding(5.dp)
            .height(183.dp),
        shape = RoundedCornerShape(8.dp),
        border = BorderStroke(1.dp, MaterialTheme.colors.onBackground),
        elevation = 8.dp
    ) {

        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Transparent),
        ) {
            Image(
                modifier = Modifier.align(Alignment.Center),
                painter = painterResource(id = R.drawable.baseline_add_circle_24),
                contentDescription = "Add new item",
                colorFilter = ColorFilter.tint(MaterialTheme.colors.primary)
            )
        }
    }
}

private const val TEXT_SCALE_REDUCTION_INTERVAL = 0.9F

@Composable
fun ResponsiveText(
    modifier: Modifier = Modifier,
    text: String,
    style: androidx.compose.ui.text.TextStyle,
    maxLines: Int = 1,
) {
    var textSize: TextUnit by remember {  mutableStateOf(style.fontSize) }

    Text(
        modifier = modifier,
        text = text,
        style = style,
        maxLines = maxLines,
        overflow = TextOverflow.Ellipsis,
        onTextLayout = { textLayoutResult: TextLayoutResult ->
            val maxCurrentLineIndex: Int = textLayoutResult.lineCount - 1

            if (textLayoutResult.isLineEllipsized(maxCurrentLineIndex)) {
                textSize = textSize.times(TEXT_SCALE_REDUCTION_INTERVAL)
            }
        },
    )
}