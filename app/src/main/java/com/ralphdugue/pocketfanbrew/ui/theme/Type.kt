package com.ralphdugue.pocketfanbrew.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.ralphdugue.pocketfanbrew.R

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily(Font(R.font.geno_regular, FontWeight.Normal)),
        fontWeight = FontWeight.Medium,
        fontSize = 12.sp
    ),
    h1 = TextStyle(
        fontFamily = FontFamily(Font(R.font.geno_shadow, FontWeight.Normal)),
        fontWeight = FontWeight.Normal,
        fontSize = 30.sp
    )
)