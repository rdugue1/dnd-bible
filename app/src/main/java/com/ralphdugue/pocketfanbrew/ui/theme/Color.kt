package com.ralphdugue.pocketfanbrew.ui.theme

import androidx.compose.ui.graphics.Color

val ThemePurple = Color(0xFF2E3090)
val LakerGold = Color(0xFFFDB927)
val Gold = Color(0xFFFFD700)
val Purple700 = Color(0xFF3700B3)
val HardWhite = Color(0xFFFFFFFF)