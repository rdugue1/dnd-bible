package com.ralphdugue.pocketfanbrew.ui.classes.list

import com.ralphdugue.phitoarch.mvi.ViewState
import com.ralphdugue.pocketfanbrew.domain.entities.classes.ClassListItemData

sealed interface ClassListState : ViewState

object Loading : ClassListState
object Empty : ClassListState
data class ShowClasses(val classes: List<ClassListItemData>): ClassListState

