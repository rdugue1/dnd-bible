package com.ralphdugue.pocketfanbrew.ui.classes.list

import com.ralphdugue.phitoarch.mvi.BaseViewModel
import com.ralphdugue.pocketfanbrew.di.modules.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@HiltViewModel
class ClassListViewModel @Inject constructor(
    classListEventHandler: ClassListIntentHandler,
    @IoDispatcher ioDispatcher: CoroutineDispatcher
):  BaseViewModel<ClassListEvent>(classListEventHandler, ioDispatcher) {

    init {
        queueIntent(ClassListEvent.UpdateClasses)
    }

    override fun initialState() = Loading

}