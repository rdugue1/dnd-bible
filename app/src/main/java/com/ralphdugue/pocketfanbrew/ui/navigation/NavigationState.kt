package com.ralphdugue.pocketfanbrew.ui.navigation

import androidx.annotation.DrawableRes
import com.ralphdugue.pocketfanbrew.R
import com.ralphdugue.pocketfanbrew.ui.destinations.ClassListScreenDestination
import com.ralphdugue.pocketfanbrew.ui.destinations.Destination
import com.ralphdugue.pocketfanbrew.ui.destinations.RaceListScreenDestination
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec

enum class DrawerDestinations(
    val label: String,
    @DrawableRes val iconRes: Int,
    val direction: DirectionDestinationSpec
) {
    Classes(
        label = "CLASSES",
        iconRes = R.drawable.rpg_classes,
        direction = ClassListScreenDestination
    ),
    Races(
        label = "RACES",
        iconRes = R.drawable.ic_baseline_person_24,
        direction = RaceListScreenDestination
    )
}

fun destinationTitle(destination: Destination) = when(destination) {
    is ClassListScreenDestination -> DrawerDestinations.Classes.label
    is RaceListScreenDestination -> DrawerDestinations.Races.label
}