package com.ralphdugue.pocketfanbrew.ui.classes.list

import com.ralphdugue.phitoarch.mvi.BaseIntent

sealed class ClassListEvent : BaseIntent {

    object UpdateClasses : ClassListEvent()

    object WatchClasses : ClassListEvent()
}
