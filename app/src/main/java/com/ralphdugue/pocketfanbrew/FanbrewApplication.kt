package com.ralphdugue.pocketfanbrew

import android.app.Application
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import dagger.hilt.android.HiltAndroidApp
import dev.ralphdugue.fanbrewdata.database.Database
import dev.ralphdugue.fanbrewdata.di.DriverFactory
import dev.ralphdugue.fanbrewdata.di.initBackend

@HiltAndroidApp
class FanbrewApplication : Application(), DriverFactory {

    override fun onCreate() {
        super.onCreate()
        initBackend(this)
    }

    override fun createDriver(): SqlDriver = AndroidSqliteDriver(
        schema = Database.Schema,
        context = applicationContext,
        name = "main.db"
    )
}