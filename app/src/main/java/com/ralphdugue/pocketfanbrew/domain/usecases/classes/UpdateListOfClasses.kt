package com.ralphdugue.pocketfanbrew.domain.usecases.classes

import com.ralphdugue.phitoarch.clean.SuspendUseCase
import dev.ralphdugue.fanbrewdata.ClassListQuery
import dev.ralphdugue.fanbrewdata.common.ApiResult
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassRepository
import javax.inject.Inject

class UpdateListOfClasses @Inject constructor(
    private val classRepository: ClassRepository
): SuspendUseCase<Any, ApiResult<ClassListQuery.Data>> {

    override suspend fun execute(param: Any) = classRepository.updateClassList()
}