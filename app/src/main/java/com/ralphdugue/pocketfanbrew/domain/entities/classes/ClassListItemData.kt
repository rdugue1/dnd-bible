package com.ralphdugue.pocketfanbrew.domain.entities.classes

data class ClassListItemData(
    val name: String,
    val imageUrl: String,
    val description: String
)