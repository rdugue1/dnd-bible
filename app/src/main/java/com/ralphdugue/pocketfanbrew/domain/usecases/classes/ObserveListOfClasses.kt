package com.ralphdugue.pocketfanbrew.domain.usecases.classes

import com.ralphdugue.phitoarch.clean.FlowUseCase
import com.ralphdugue.pocketfanbrew.domain.entities.classes.ClassListItemData
import com.ralphdugue.pocketfanbrew.domain.mappers.ClassMappers
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveListOfClasses @Inject constructor(
    private val classRepository: ClassRepository
): FlowUseCase<Any, List<ClassListItemData>> {
    override fun execute(param: Any): Flow<List<ClassListItemData>> =
        classRepository.watchClassList()
            .map { class_ -> class_.map { ClassMappers.map(it) } }
}