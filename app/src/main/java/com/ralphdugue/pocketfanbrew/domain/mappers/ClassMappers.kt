package com.ralphdugue.pocketfanbrew.domain.mappers

import com.ralphdugue.phitoarch.clean.Mapper
import com.ralphdugue.pocketfanbrew.domain.entities.classes.ClassListItemData
import devralphduguefanbrewdata.classes.Class_list_class


object ClassMappers : Mapper<Class_list_class, ClassListItemData> {

    override fun map(_class: Class_list_class): ClassListItemData = with(_class) {
        ClassListItemData(
            name = name,
            imageUrl = "",
            description = hit_dice + saving_throws
        )
    }
}